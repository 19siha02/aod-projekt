package se.hig.aod.projekt.algoritm;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Abstrakt klass som definierar alla baskraven för att algoritmen ska fungera.
 * 
 * @author Simon Hall
 * @author Johannes Gyllenskepp
 * @version 2021-01-11
 */



public abstract class AbstractNode implements Comparable<AbstractNode>{

	/**
	 * Namn för noden
	 */
	private String name;
	
	/**
	 * Länkad lista för kortaste vägen där ordningen är av intresse
	 */
	private List<AbstractNode> shortestPath = new LinkedList<>();
    
	/**
	 * Distans till noden från startpunkten.
	 */
    private Integer distance = Integer.MAX_VALUE;
    
    /**
	 *	Hashmap för grannar, lämpligt när grannar ska ha en distans
     * 
     */
    Map<AbstractNode, Integer> adjacentNodes = new HashMap<>(8);
    

    
    /**
     * Envägskoppling, a är granne med b men b är inte granne med a. T. ex enkelriktad väg.
     * @param destination - grannnoden
     * @param distance - distansen, weight, till grannen 
     */
    public void addDestination(AbstractNode destination, int distance) {
        adjacentNodes.put(destination, distance);
    }
    
    /**
     * Tvåvägskoppling, a & b är grannar med varandra
     * @param destination - grannnoden
     * @param distance - distansen, weight, till grannen 
     */
    public static void connectNeighbour(AbstractNode nodeA, AbstractNode nodeB, int distance) {
		nodeA.addDestination(nodeB, distance);
		nodeB.addDestination(nodeA, distance);
		
	}
    /**
     * Hämta distansen från startpunkten till noden
     * @return Integer med distansen
     */
    public Integer getDistance() {
		return distance;
	}

    /**
     * Sätt distansen till noden från startpunkten
     * @param distance - Integer med distansen
     */
	public void setDistance(Integer distance) {
		this.distance = distance;
	}

	/**
	 * Hämta alla kopplade grannnoder.
	 * @return En Map med alla grannnoder
	 */
	public Map<AbstractNode, Integer> getAdjacentNodes() {
		return adjacentNodes;
	}


	/**
	 * Hämta listan med den kortaste vägen till noden från startpunkten
	 * @return En List med noderna i ordning från startpunkten till noden
	 */
	public List<AbstractNode> getShortestPath() {
		return shortestPath;
	}

	/**
	 * Används av algoritmen för att sätta den hittills kortaste vägen till noden
	 * @param shortestPath
	 */
	public void setShortestPath(List<AbstractNode> shortestPath) {
		this.shortestPath = shortestPath;
	}

	/**
	 * 
	 * @return Namnet på noden
	 */
	public String getName() {
		return name;
	}

	/**
	 * 
	 * @param name - Namnet på noden
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	public int compareTo(AbstractNode o) {
		if(o.getDistance()>distance)
			return -1;
		else if(o.getDistance()<distance )
			return 1;
		return 0;
	}
}
