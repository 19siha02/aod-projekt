package se.hig.aod.projekt.algoritm;

import java.util.Set;

/**
 * Interface för funktionera som en graf måste ha för att vår implmementation av Djikstras ska fungera.
 * 
 * @author Simon Hall
 * @author Johannes Gyllenskepp
 * @version 2021-01-11
 *
 * @param <T> - Typen T som ärver AbstractNode
 */
public interface Graph<T extends AbstractNode> {

    /**
     * Lägg till en nod i grafen.
     * @param node - Noden som ska läggas till
     */
    public void addNode(T node);
    
    /**
     * Hämta alla noder i grafen.
     * @return Ett set med alla noder i grafen
     */
	public Set<T> getNodes() ;
	
	/**
	 * Nollställ grafen så Algoritmen kan köras på samma graf igen.
	 */
	public void resetGraph();
	

	
    

}
