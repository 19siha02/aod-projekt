package se.hig.aod.projekt.algoritm;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map.Entry;
import java.util.PriorityQueue;
import java.util.Set;

/**
 * Implementation av Djikstras Algoritm.
 * Inspirerad av Baeldung https://www.baeldung.com/author/baeldung/
 * @author Simon Hall
 * @author Johannes Gyllenskepp
 * @version 2021-01-11
 */


public class Dijkstra {
	
	/** Hitta kortaste vägen till alla noder i grafen från en startpunkt
	 * 
	 * @param graph - Grafen vars noder ska beräknas
	 * @param startNode - Noden som är startpunken där alla distanser räknas ifrån
	 */
	public static void findShortestPathToAll(Graph<? extends AbstractNode> graph, AbstractNode startNode) {
		graph.resetGraph();
		startNode.setDistance(0);
		
		//Hashsets för snabb kontroll om en nod redan är besökt
		PriorityQueue<AbstractNode> unvisited = new PriorityQueue<>();
		Set<AbstractNode> visited = new HashSet<>();
		unvisited.add(startNode);

		
		//Sätter distance som infinite default i abstractNode
		while(unvisited.size() != 0) {
			//Markera noden som besökt
			AbstractNode currentNode = unvisited.remove();
			visited.add(currentNode);
			
			//Gå igenom alla grannnoder
	        for (Entry <AbstractNode, Integer> neighbour: 
	          currentNode.getAdjacentNodes().entrySet()) {
	            AbstractNode neighbNode = neighbour.getKey();
	            Integer neighbDistance = neighbour.getValue();
	            //Om en grannnod inte redan är besökt så läggs den till bland de obesökta
	            if (!visited.contains(neighbNode)) {
	                calculateMinimumDistance(neighbNode, neighbDistance, currentNode);
	                unvisited.add(neighbNode);
	            }
	        }
	
	        
		}
		
		
	}
	/**
	 * Hitta kortaste vägen mellan en startpunkt och en slut punkt
	 * @param graph - Grafen vars noder ska beräknas
	 * @param startNode - Noden som är startpunken där alla distanser räknas ifrån
	 * @param goal - Målnoden
	 */
	public static void findShortestPathToGoal(Graph<? extends AbstractNode> graph, AbstractNode startNode, AbstractNode goal) {
		graph.resetGraph();
		startNode.setDistance(0);
		
		PriorityQueue<AbstractNode> unvisited = new PriorityQueue<>();
		Set<AbstractNode>  visited = new HashSet<>();
		boolean found = false;		
		unvisited.add(startNode);
		
		//Loopa så länge vi vi har obesökta noder eller hittat målet
		while(unvisited.size() != 0 && !found) {
			AbstractNode currentNode = unvisited.remove();
			//Markera noden som beökt
			unvisited.remove(currentNode);
			visited.add(currentNode);
			
			//Gå igenom alla grannnoder
	        for (Entry <AbstractNode, Integer> neighbour: 
	          currentNode.getAdjacentNodes().entrySet()) {
	            AbstractNode neighbNode = neighbour.getKey();
	            Integer neighbDistance = neighbour.getValue();
	            //Om en grannnod inte redan är besökt så läggs den till bland de obesökta
	            if (!visited.contains(neighbNode)) {
	                calculateMinimumDistance(neighbNode, neighbDistance, currentNode);
	                unvisited.add(neighbNode);
	            }
	        }
	                
	      //Om målnoden har hittats så avbryter vi
	        if(currentNode.equals(goal))
				found=true;
		}
			
	}
	
	
	
	/**
	 * Räkna ut kortaste distansen för en nod från startpunkten
	 * @param neighNode Noden vars distans ska beräknas
	 * @param distance Distansen från grannen
	 * @param sourceNode Grannnoden
	 */
	private static void calculateMinimumDistance(AbstractNode neighNode,
			  Integer distance, AbstractNode sourceNode) {
			    Integer sourceDistance = sourceNode.getDistance();
			    if (sourceDistance + distance < neighNode.getDistance()) {
			        neighNode.setDistance(sourceDistance + distance);
			        LinkedList<AbstractNode> shortestPath = new LinkedList<>(sourceNode.getShortestPath());
			        shortestPath.addFirst(sourceNode);
			        neighNode.setShortestPath(shortestPath);
			    }
			}
}
