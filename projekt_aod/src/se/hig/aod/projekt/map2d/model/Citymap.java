package se.hig.aod.projekt.map2d.model;

import se.hig.aod.projekt.algoritm.Dijkstra;

/**
 * Karta av centrala gävle. 
 * @author Simon Hall
 * @author Johannes Gyllenskepp
 * @version 2021-01-11
 */
public class Citymap {

	Graph2D graph;
	public Graph2D getGraph() {
		return graph;
		
	}
	
	public Citymap() {
		graph = new Graph2D();
		MapNode2D node1 = new MapNode2D(60.677151779327396, 17.149288603053133,"1");
		MapNode2D node2 = new MapNode2D(60.675789605137865, 17.15021999606137,"2");
		MapNode2D node3 = new MapNode2D(60.67502101352486, 17.150883454094636,"3");
		MapNode2D node4 = new MapNode2D(60.67463358640464, 17.14908446211982,"4");
		MapNode2D node5 = new MapNode2D(60.674327390703844, 17.148178586728246,"5");
		MapNode2D node6 = new MapNode2D(60.67404618800454, 17.147106846828358,"6");
		MapNode2D node7 = new MapNode2D(60.67378372993533, 17.146047865736797,"7");
		MapNode2D node8 = new MapNode2D(60.673533767879725, 17.14511647272856,"8");
		MapNode2D node9 = new MapNode2D(60.67302758877169, 17.14329196313708,"9");
		MapNode2D node10 = new MapNode2D(60.67275262394454, 17.142169188003866,"10");
		MapNode2D node11 = new MapNode2D(60.67254639878271, 17.13966846157079,"11");
		MapNode2D node12 = new MapNode2D(60.67404618800454, 17.138099128145953,"12");
		MapNode2D node13 = new MapNode2D(60.673546266028616, 17.13619806570448,"13");
		MapNode2D node14 = new MapNode2D(60.674489862254745, 17.135164602229587,"14");
		MapNode2D node15 = new MapNode2D(60.675383441125156, 17.134284244454676,"15");
		MapNode2D node16 = new MapNode2D(60.67584584282793, 17.136108754046155,"16");
		MapNode2D node17 = new MapNode2D(60.67638946876588, 17.13811188695428,"17");
		MapNode2D node18 = new MapNode2D(60.67670814177076, 17.13928569732083,"18");
		MapNode2D node19 = new MapNode2D(60.67718926954488, 17.141327106653954,"19");
		MapNode2D node20 = new MapNode2D(60.67739546496053, 17.142169188003866,"20");
		MapNode2D node21 = new MapNode2D(60.67768913493848, 17.143189892670428,"21");
		MapNode2D node22 = new MapNode2D(60.67798280223665, 17.14426163257032,"22");
		MapNode2D node23 = new MapNode2D(60.677120537446115, 17.145193025578553,"23");
		MapNode2D node24 = new MapNode2D(60.67735797498321, 17.146098900970127,"24");
		MapNode2D node25 = new MapNode2D(60.67765164530329, 17.147106846828358,"25");
		MapNode2D node26 = new MapNode2D(60.676745632505074, 17.148063757453258,"26");
		MapNode2D node27 = new MapNode2D(60.67650194195144, 17.147017535170033,"27");
		MapNode2D node28 = new MapNode2D(60.67625824955263, 17.146149936203454,"28");
		MapNode2D node29 = new MapNode2D(60.67594582070101, 17.14507819630357,"29");
		MapNode2D node30 = new MapNode2D(60.67569587543601, 17.14404473282867,"30");
		MapNode2D node31 = new MapNode2D(60.676551929907745, 17.143113339820435,"31");
		MapNode2D node32 = new MapNode2D(60.676839359149774, 17.144134044487,"32");
		MapNode2D node33 = new MapNode2D(60.67449611114416, 17.146724082578395,"33");
		MapNode2D node34 = new MapNode2D(60.674746065726396, 17.14775754605329,"34");
		MapNode2D node35 = new MapNode2D(60.67498352078156, 17.148714456678192,"35");
		MapNode2D node36 = new MapNode2D(60.67633323202571, 17.142156429195534,"36");
		MapNode2D node37 = new MapNode2D(60.67546467433774, 17.143062304587104,"37");
		MapNode2D node38 = new MapNode2D(60.67388996322114, 17.144746467286932,"38");
		MapNode2D node39 = new MapNode2D(60.67340878612399, 17.142819887228796,"39");
		MapNode2D node40 = new MapNode2D(60.67310882793023, 17.14177366494557,"40");
		MapNode2D node41 = new MapNode2D(60.67357751137956, 17.1398726025041,"41");
		MapNode2D node42 = new MapNode2D(60.67440862658181, 17.139017762345855,"42");
		MapNode2D node43 = new MapNode2D(60.67547717173687, 17.139107074004183,"43");
		MapNode2D node44 = new MapNode2D(60.675827096942164, 17.140280884370725,"44");	
		MapNode2D node45 = new MapNode2D(60.67605204685085, 17.141135724528972,"45");
		MapNode2D node46 = new MapNode2D(60.675264715291775, 17.13811188695428,"46");
		MapNode2D node47 = new MapNode2D(60.67498352078156, 17.13707842347939,"47");
		
		MapNode2D node48 = new MapNode2D(60.675713516751806, 17.144664176618168, "Buddys City");
		MapNode2D node49 = new MapNode2D(60.67506249009201, 17.145097907968786,"Bro Burger Bar");
		MapNode2D node50 = new MapNode2D(60.674250309386146, 17.144246460454916,"Helt Enkelt Bar & Kök");
		MapNode2D node51 = new MapNode2D(60.67400035095394, 17.14457181005069,"Pitchers");
		MapNode2D node52 = new MapNode2D(60.67339895395918, 17.144256400966015,"Neo Neo");
		MapNode2D node53 = new MapNode2D(60.67346081685218, 17.14311513823512,"Grillo");
		MapNode2D node54 = new MapNode2D(60.67336360368206, 17.14257833878912,"Church Street Saloon");
		MapNode2D node55 = new MapNode2D(60.67353663548847, 17.136327272726525,"Sushi Kultur");
		MapNode2D node56 = new MapNode2D(60.67421936301386, 17.138064200210582,"American Pizza Place");
		MapNode2D node57 = new MapNode2D(60.674689947893924, 17.13978286065921,"Joe's Burger");
		MapNode2D node58 = new MapNode2D(60.67524664334105, 17.140751181055762,"La Mancha Steakhouse");
		MapNode2D node59 = new MapNode2D(60.675675168548544, 17.140326039223872,"Brasserie Absint");
		MapNode2D node60 = new MapNode2D(60.67587771911527, 17.142613323683786,"Épi Bageri & Café");
		MapNode2D node61 = new MapNode2D(60.67568855862404, 17.142771574009462,"Verovin");
		MapNode2D node62 = new MapNode2D(60.67636924419715, 17.146124823691913,"Indian Palace");
		MapNode2D node63 = new MapNode2D(60.67676764916587, 17.14850648829719,"Tennstopet");
		MapNode2D node64 = new MapNode2D(60.67473884506292, 17.149609026256524,"Grands Veranda");
		MapNode2D node65 = new MapNode2D(60.6754299361746, 17.15022732049993,"Trivs Pub & Restaurang");
		MapNode2D node66 = new MapNode2D(60.67730590112804, 17.142420075141313,"The Frozen Mexican");
		MapNode2D node67 = new MapNode2D(60.677536074685655, 17.145837906490502,"Bilagan");

		MapNode2D.connectNeighbour(node48, node29, 39);
		MapNode2D.connectNeighbour(node48, node30, 34);
		MapNode2D.connectNeighbour(node49, node30, 83);
		MapNode2D.connectNeighbour(node49, node7, 152);
		MapNode2D.connectNeighbour(node50, node38, 41);
		MapNode2D.connectNeighbour(node50, node37, 149);
		MapNode2D.connectNeighbour(node51, node50, 34);
		MapNode2D.connectNeighbour(node51, node38, 12);
		MapNode2D.connectNeighbour(node52, node8, 50);
		MapNode2D.connectNeighbour(node52, node9, 70);
		//MapNode2D.connectNeighbour(node53, node38, 102);
		MapNode2D.connectNeighbour(node53, node39, 17);
		MapNode2D.connectNeighbour(node54, node39, 14);
		MapNode2D.connectNeighbour(node54, node40, 53);
		MapNode2D.connectNeighbour(node55, node13, 6);
		//MapNode2D.connectNeighbour(node55, node12, 108);
		MapNode2D.connectNeighbour(node56, node12, 13);
		//MapNode2D.connectNeighbour(node56, node42, 55);
		MapNode2D.connectNeighbour(node57, node42, 52);
		MapNode2D.connectNeighbour(node57, node43, 102);
		//MapNode2D.connectNeighbour(node58, node44, 68);
		MapNode2D.connectNeighbour(node59, node58, 52);
		MapNode2D.connectNeighbour(node59, node44, 15);
		MapNode2D.connectNeighbour(node60, node36, 55);
		MapNode2D.connectNeighbour(node60, node61, 23);
		MapNode2D.connectNeighbour(node61, node37, 33);
		MapNode2D.connectNeighbour(node62, node28, 11);
		//MapNode2D.connectNeighbour(node62, node23, 100);
		MapNode2D.connectNeighbour(node63, node26, 23);
		MapNode2D.connectNeighbour(node63, node1, 62);
		MapNode2D.connectNeighbour(node64, node3, 79);
		MapNode2D.connectNeighbour(node64, node4, 30);
		MapNode2D.connectNeighbour(node65, node35, 94);
		MapNode2D.connectNeighbour(node65, node26, 217);
		MapNode2D.connectNeighbour(node66, node20, 19);
		//MapNode2D.connectNeighbour(node66, node31, 90);
		MapNode2D.connectNeighbour(node67, node24, 26);
		
		
		
		MapNode2D.connectNeighbour(node1, node2, 150);
		MapNode2D.connectNeighbour(node3, node2, 100);
		MapNode2D.connectNeighbour(node3, node4, 105);
		MapNode2D.connectNeighbour(node4, node5, 60);
		MapNode2D.connectNeighbour(node6, node5, 64);
		MapNode2D.connectNeighbour(node6, node7, 70);
		MapNode2D.connectNeighbour(node8, node7, 55);
		MapNode2D.connectNeighbour(node8, node9, 120);
		MapNode2D.connectNeighbour(node9, node10, 68);
		MapNode2D.connectNeighbour(node10, node11, 168);
		MapNode2D.connectNeighbour(node11, node12, 200);
		MapNode2D.connectNeighbour(node12, node13, 115);
		MapNode2D.connectNeighbour(node13, node14, 110);
		MapNode2D.connectNeighbour(node15, node14, 110);
		MapNode2D.connectNeighbour(node15, node16, 113);
		MapNode2D.connectNeighbour(node17, node16, 128);
		MapNode2D.connectNeighbour(node17, node18, 72);
		MapNode2D.connectNeighbour(node18, node19, 120);
		MapNode2D.connectNeighbour(node19, node20, 58);
		MapNode2D.connectNeighbour(node21, node20, 65);
		MapNode2D.connectNeighbour(node21, node22, 65);
		MapNode2D.connectNeighbour(node22, node23, 108);
		MapNode2D.connectNeighbour(node24, node23, 55);
		MapNode2D.connectNeighbour(node24, node25, 66);
		MapNode2D.connectNeighbour(node25, node26, 110);
		MapNode2D.connectNeighbour(node26, node27, 61);
		MapNode2D.connectNeighbour(node27, node28, 55);
		MapNode2D.connectNeighbour(node28, node29, 69);
		MapNode2D.connectNeighbour(node29, node30, 61);
		MapNode2D.connectNeighbour(node31, node30, 107);
		MapNode2D.connectNeighbour(node31, node32, 63);
		MapNode2D.connectNeighbour(node33, node34, 67);
		MapNode2D.connectNeighbour(node35, node34, 59);
		MapNode2D.connectNeighbour(node36, node37, 114);
		MapNode2D.connectNeighbour(node37, node38, 190);
		MapNode2D.connectNeighbour(node38, node39, 117);
		MapNode2D.connectNeighbour(node39, node40, 70);
		MapNode2D.connectNeighbour(node40, node41, 137);
		//MapNode2D.connectNeighbour(node42, node43, 175);
		MapNode2D.connectNeighbour(node43, node44, 67);
		MapNode2D.connectNeighbour(node45, node44, 57);
		MapNode2D.connectNeighbour(node43, node46, 65);
		MapNode2D.connectNeighbour(node46, node47, 62);
		
		MapNode2D.connectNeighbour(node1, node26, 82);
		MapNode2D.connectNeighbour(node24, node27, 108);
		MapNode2D.connectNeighbour(node23, node28, 108);
		MapNode2D.connectNeighbour(node32, node29, 108);
		MapNode2D.connectNeighbour(node23, node32, 65);
		MapNode2D.connectNeighbour(node20, node31, 110);
		MapNode2D.connectNeighbour(node19, node36, 108);
		MapNode2D.connectNeighbour(node36, node45, 65);
		MapNode2D.connectNeighbour(node18, node44, 105);
		MapNode2D.connectNeighbour(node17, node43, 110);
		MapNode2D.connectNeighbour(node16, node47, 105);
		MapNode2D.connectNeighbour(node14, node47, 115);
		MapNode2D.connectNeighbour(node12, node47, 110);
		MapNode2D.connectNeighbour(node12, node42, 60);
		MapNode2D.connectNeighbour(node46, node42, 108);
		MapNode2D.connectNeighbour(node11, node13, 270);
		MapNode2D.connectNeighbour(node10, node40, 45);
		MapNode2D.connectNeighbour(node9, node39, 45);
		MapNode2D.connectNeighbour(node8, node38, 45);
		MapNode2D.connectNeighbour(node37, node45, 177);
		MapNode2D.connectNeighbour(node30, node37, 54);
		MapNode2D.connectNeighbour(node29, node33, 188);
		MapNode2D.connectNeighbour(node30, node7, 235);
		MapNode2D.connectNeighbour(node33, node6, 48);
		MapNode2D.connectNeighbour(node34, node5, 45);
		MapNode2D.connectNeighbour(node35, node4, 45);
		MapNode2D.connectNeighbour(node28, node34, 190);
		MapNode2D.connectNeighbour(node27, node35, 190);
		
		graph.addNode(node1);
		graph.addNode(node2);
		graph.addNode(node3);
		graph.addNode(node4);
		graph.addNode(node5);
		graph.addNode(node6);
		graph.addNode(node7);
		graph.addNode(node8);
		graph.addNode(node9);
		graph.addNode(node10);
		graph.addNode(node11);
		graph.addNode(node12);
		graph.addNode(node13);
		graph.addNode(node14);
		graph.addNode(node15);
		graph.addNode(node16);
		graph.addNode(node17);
		graph.addNode(node18);
		graph.addNode(node19);
		graph.addNode(node20);
		graph.addNode(node21);
		graph.addNode(node22);
		graph.addNode(node23);
		graph.addNode(node24);
		graph.addNode(node25);
		graph.addNode(node26);
		graph.addNode(node27);
		graph.addNode(node28);
		graph.addNode(node29);
		graph.addNode(node30);		
		graph.addNode(node31);
		graph.addNode(node32);
		graph.addNode(node33);
		graph.addNode(node34);
		graph.addNode(node35);
		graph.addNode(node36);
		graph.addNode(node37);
		graph.addNode(node38);
		graph.addNode(node39);
		graph.addNode(node40);
		graph.addNode(node41);
		graph.addNode(node42);
		graph.addNode(node43);	
		graph.addNode(node44);
		graph.addNode(node45);
		graph.addNode(node46);
		graph.addNode(node47);
		graph.addNode(node48);
		graph.addNode(node49);
		graph.addNode(node50);
		graph.addNode(node51);
		graph.addNode(node52);
		graph.addNode(node53);
		graph.addNode(node54);
		graph.addNode(node55);
		graph.addNode(node56);
		graph.addNode(node57);
		graph.addNode(node58);
		graph.addNode(node59);
		graph.addNode(node60);
		graph.addNode(node61);
		graph.addNode(node62);
		graph.addNode(node63);
		graph.addNode(node64);
		graph.addNode(node65);
		graph.addNode(node66);
		graph.addNode(node67);

		/*
		long time = 0;
		for(int i = 0; i<20;i++) {
			time += testSpeed(node67, node67,true );
		}
		time = time/20;
		System.out.println(time);
		time = 0;
		for(int i = 0; i<20;i++) {
			time += testSpeed(node67, node67,false );
		}
		time = time/20;
		System.out.println(time);
		*/
	}
	
	public long testSpeed(MapNode2D start, MapNode2D end, boolean mode) {
		long tmp = System.nanoTime();
		if(mode)
			Dijkstra.findShortestPathToAll(graph, start);
		else
			Dijkstra.findShortestPathToGoal(graph, start, end);
		return System.nanoTime()-tmp;
	}

}
