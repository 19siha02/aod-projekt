package se.hig.aod.projekt.map2d.model;

import se.hig.aod.projekt.algoritm.AbstractNode;

/**
 * Implementation av AbstractNode för representation av en kartnod.
 * @author Simon Hall
 * @author Johannes Gyllenskepp
 * @version 2021-01-11
 */
public class MapNode2D extends AbstractNode{
	
	private double longitude;
	private double latitude;
	
	public MapNode2D(double lat, double lon, String name) {
		super();
		this.latitude = lat;
		this.longitude = lon;
		this.setName(name);
	}

	public double getLatitude() {
		return latitude;
	}

	public double getLongitude() {
		return longitude;
	}
	
	public String toString() {
		return getName();
	}

	
}
