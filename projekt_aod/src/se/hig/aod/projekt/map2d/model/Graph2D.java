package se.hig.aod.projekt.map2d.model;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;

import se.hig.aod.projekt.algoritm.Graph;

/**
 * En graf för att hålla i en samling av MapNode2D-noder
 * 
 * @author Simon Hall
 * @author Johannes Gyllenskepp
 * @version 2021-01-11
 */

public class Graph2D implements Graph<MapNode2D>{

	private Set<MapNode2D> nodes = new HashSet<>();
    
   

    public Set<MapNode2D> getNodes() {
		return nodes;
    }

    public void resetGraph() {
    	for(MapNode2D n:nodes) {
    		n.setDistance(Integer.MAX_VALUE);
    		n.setShortestPath(new LinkedList<>());
    	}
    }

	public void addNode(MapNode2D node) {
		nodes.add(node);
		
	}

	
	
}
