package se.hig.aod.projekt.map2d.gui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;

import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;

import se.hig.aod.projekt.algoritm.AbstractNode;
import se.hig.aod.projekt.algoritm.Dijkstra;
import se.hig.aod.projekt.map2d.model.Citymap;
import se.hig.aod.projekt.map2d.model.Graph2D;
import se.hig.aod.projekt.map2d.model.MapNode2D;


/**
 * GUI för att styra och visualisera resultatet av algoritmen.
 * @author Simon Hall
 * @author Johannes Gyllenskepp
 * @version 2021-01-11
 */
public class TestGUI {

	JFrame mainWindow = new JFrame("");
	MapNode2D startNode = null;
	MapNode2D endNode = null;
	JLabel startLabel = new JLabel("Start");
	JLabel endLabel = new JLabel("End");
	Graph2D graph;
	
	public TestGUI() {
		createGUI();
		mainWindow.setVisible(true);
		mainWindow.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
	}

	
	private void createGUI() {

		//Ladda in noderna
		graph = new Citymap().getGraph();
		
		//Skapa en JList med noderna
		DefaultListModel<MapNode2D> dlm = new DefaultListModel<>();
		for(MapNode2D n:graph.getNodes()) {
			//Temporär filtrering av gatnoderna
			if(n.getName().length()>3)
				dlm.addElement(n);
		}
		JList<MapNode2D> jlist = new JList<>(dlm);
		JScrollPane listScroll = new JScrollPane(jlist);
		
		//Skapa knapparna för styrning
		JCheckBox drawAll = new JCheckBox("Draw All",true);
		JButton setStart = new JButton("Set start");
		JButton setEnd = new JButton("Set end");
		JButton go = new JButton("Go");
		
		//Skapa lyssnare för knapparna
		setStart.addActionListener(e -> {
			if(jlist.getSelectedValue()!=null) {
				startNode = jlist.getSelectedValue();
				startLabel.setText("Start: "+jlist.getSelectedValue().getName());
			}
			
		});
		
		setEnd.addActionListener(e -> {
			if(jlist.getSelectedValue()!=null) {
				endNode = jlist.getSelectedValue();
				endLabel.setText("End: "+jlist.getSelectedValue().getName());
			}	
		});
		
		go.addActionListener(e -> {
			if (startNode != null && endNode != null) {
				if(drawAll.isSelected()) {
					Dijkstra.findShortestPathToAll(graph, startNode);
					showMap(graph, startNode, endNode, drawAll.isSelected());
				}
				else {
					Dijkstra.findShortestPathToGoal(graph, startNode, endNode);
					showMap(graph, startNode, endNode, drawAll.isSelected());
				}
					
				
			}
			else
				JOptionPane.showMessageDialog(new JFrame(), "Välj start & end","Fel",JOptionPane.ERROR_MESSAGE);
		});
		
		//Placera alla kompenenter
		mainWindow.getContentPane().setLayout(new BorderLayout());
		JPanel eastPanel = new JPanel(new GridLayout(3, 2));
		eastPanel.add(startLabel);
		eastPanel.add(setStart);
		eastPanel.add(endLabel);
		eastPanel.add(setEnd);
		eastPanel.add(drawAll);
		eastPanel.add(go);
		mainWindow.add(eastPanel, BorderLayout.EAST);
		mainWindow.add(listScroll, BorderLayout.WEST);
		mainWindow.setSize(new Dimension(800,600));

		
	}
	private void showMap(Graph2D graph, MapNode2D startNode, MapNode2D endNode, boolean drawAll) {


		ImageIcon bild = new ImageIcon("mapcity.png");
		
		//Skapa kartan med noderna och vägarna utritade
		JPanel map = new MapPanel(bild, graph, endNode,drawAll, 17.15595449415041, 17.12602002939064, 60.67892275885302, 60.670536256833095);		
		JFrame mapFrame = new JFrame("Map");
		mapFrame.add(map);
		mapFrame.setSize(new Dimension(bild.getIconWidth()+20, bild.getIconHeight()+40));
		mapFrame.setVisible(true);
		
		//Skapa ett meddelande med kortaste vägen samt dess distans
		String pathString = "";
		for(AbstractNode n:endNode.getShortestPath()) {
			pathString = n.getName() + " → " +pathString;
		}
		pathString = pathString+endNode.getName();
		JOptionPane.showMessageDialog(mapFrame, "Kortaste vägen är "+endNode.getDistance()+"m"+"\nVägen: "+pathString);
	}
	public static void main(String[] args) {
		SwingUtilities.invokeLater(() -> new TestGUI());
	}
	
}
