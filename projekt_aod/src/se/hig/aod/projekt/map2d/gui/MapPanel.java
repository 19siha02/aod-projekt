package se.hig.aod.projekt.map2d.gui;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.util.Iterator;
import java.util.Set;
import java.util.Map.Entry;
import javax.swing.ImageIcon;
import javax.swing.JPanel;

import se.hig.aod.projekt.algoritm.AbstractNode;
import se.hig.aod.projekt.map2d.model.Graph2D;
import se.hig.aod.projekt.map2d.model.MapNode2D;

/**
 * Klass för att rita ut noderna på en kartbild. Bygger på en tidigare version
 * som användes i OODP-kursen.
 * 
 * @author Simon Hall
 * @author Johannes Gyllenskepp
 * @version 2021-01-11
 */

public class MapPanel extends JPanel {

	
	private static final long serialVersionUID = 1L;
	private ImageIcon imageIcon;
	private Image image;
	private Set<MapNode2D> set;
	private double latDiff;
	private double longDiff;
	private double maxLong;
	private double maxLat;
	private int[] xPoints;
	private int[] yPoints;
	private int[] shxPoints;
	private int[] shyPoints;
	private boolean drawAll;
	private Graph2D gg;

	private MapNode2D goal;

	public MapPanel(ImageIcon imageIcon, Graph2D graph, MapNode2D goal, boolean drawAll, double maxLong, double minLong,
			double maxLat, double minLat) {
		this.drawAll = drawAll;
		this.imageIcon = imageIcon;
		image = imageIcon.getImage();
		JPanel panel = new JPanel();
		gg = graph;
		this.add(panel);
		this.goal = goal;

		set = graph.getNodes();
		if (set.size() > 0) {

			xPoints = new int[set.size()];
			yPoints = new int[set.size()];

			this.maxLong = maxLong;
			this.maxLat = maxLat;

			longDiff = maxLong - minLong;
			latDiff = maxLat - minLat;
			MapNode2D currentNode = null;
			Iterator<MapNode2D> iter = set.iterator();
			for (int i = 0; i < set.size(); i++) {
				currentNode = iter.next();
				yPoints[i] = convertCoordY(currentNode.getLatitude());
				xPoints[i] = convertCoordX(currentNode.getLongitude());

			}
			
			//Skapa x-y för kortaste vägen
			shxPoints = new int[goal.getShortestPath().size() + 1];
			shyPoints = new int[goal.getShortestPath().size() + 1];
			int shortestPathIndex = goal.getShortestPath().size();
			shyPoints[shortestPathIndex] = convertCoordY(goal.getLatitude());
			shxPoints[shortestPathIndex] = convertCoordX(goal.getLongitude());
			for (AbstractNode node : goal.getShortestPath()) {
				shortestPathIndex--;
				MapNode2D ads = (MapNode2D) node;
				shyPoints[shortestPathIndex] = convertCoordY(ads.getLatitude());
				shxPoints[shortestPathIndex] = convertCoordX(ads.getLongitude());
				
			}
			

		}

	}

	
	private int convertCoordY(double lat) {
		return ((int) (((maxLat - lat) / latDiff)
				* image.getHeight(imageIcon.getImageObserver())));
	}
	
	private int convertCoordX(double lon) {
		return (image.getWidth(imageIcon.getImageObserver())
				- (int) (((maxLong - lon) / longDiff)
						* image.getWidth(imageIcon.getImageObserver())));
	}
	
	@Override
	public void paintComponent(Graphics g) {

		g.drawImage(image, 0, 0, this);

		if (set.size() > 0) {

			Graphics2D g2 = (Graphics2D) g;
			g2.setStroke(new BasicStroke(3));
			g2.setColor(Color.red);

			// Rita ut kopplingar mellan grann-noder
			for (MapNode2D node : gg.getNodes()) {
				if (drawAll || node.getDistance() < goal.getShortestPath().get(0).getDistance()) {
					for (Entry<AbstractNode, Integer> adjacencyPair : node.getAdjacentNodes().entrySet()) {
						g2.drawLine(
								convertCoordX(node.getLongitude()),
								convertCoordY(node.getLatitude()),
								convertCoordX(((MapNode2D)adjacencyPair.getKey()).getLongitude()),
								convertCoordY(((MapNode2D)adjacencyPair.getKey()).getLatitude()));
					}
				}
			}
			
			//Rita ut kortaste vägen
			g2.setColor(Color.green);
			g2.drawPolyline(shxPoints, shyPoints, shxPoints.length);
			g.setColor(Color.black);
			for (int i = 0; i < xPoints.length; i++)
				g.fillRect(xPoints[i] - 4, yPoints[i] - 4, 8, 8);

		}

	}

}
